'use strict'

/**
 * @author WXP
 * @description 区域映射
 */
const {
    AreaMap: AreaMapDb
} = require('../model');
const _ = require('lodash');

let AreaMap = module.exports;

AreaMap.findOrCreate = function ({
    name,
    type,
    level
}) {
    if (!Array.isArray(name)) {
        name = [name];
    }
    return AreaMapDb.findAll({
            where: {
                name: name,
                level: level,
                type: type
            },
            raw: true
        })
        .then((data) => {
            if (data.length == name.length) {
                return data;
            } else {
                let notContainData = _.without(name, _.map(data, 'name'));
                let newData = [];
                for (let notContain of notContainData) {
                    newData.push({
                        name: notContain,
                        level: level,
                        type: type
                    })
                }
                return AreaMapDb.bulkCreate(newData)
                    .then(() => {
                        return AreaMap.findOrCreate({
                            name: name,
                            type: type,
                            level: level
                        });
                    })
            }
        })
};

// AreaMap.findOrCreate({
//     name: ['1'],
//     level: '2',
//     type: '3'
// })
// .then((data) => {
//     console.log(data);
// })
// .catch((error) => {
//     console.error(error);
// })