const request = require('request');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const Promise = require('bluebird');
const iconv = require('iconv-lite');
const fsStat = Promise.promisify(fs.stat);
const fsMkdir = Promise.promisify(fs.mkdir);
const fsReadDir = Promise.promisify(fs.readdir);
const ORIGNAL_DATA_PATH = path.join(__dirname, './orignalData');
const DATA_PATH = path.join(__dirname, './data');
const jsdom = require('jsdom');
const jquery = fs.readFileSync(path.join(__dirname, './jquery-3.2.0.js'), 'utf8');
const _ = require('lodash');
const {
    AreaMap
} = require('./service');
const {
    DailyData: DailyDataDb
} = require('./model');

const getConfig = function (date) {
    let tableHeader = {
        ' 区县': 'name',
        '住宅套数': 'houseCount',
        '住宅面积（㎡）': 'houseArea',
        '总成交套数': 'allCount',
        '总成交面积（㎡）': 'allArea'
    }
    if (date <= '2017-04-19') {
        return {
            dataMap: {
                newCount: 'body > div.S > div.A > a > span:nth-child(1)',
                newArea: 'body > div.S > div.A > a > span:nth-child(2)',
                newBusinessCount: 'body > div.S > div.A > a > span:nth-child(3)',
                newBusinessArea: 'body > div.S > div.A > a > span:nth-child(4)',
                oldCount: 'body > div.S > div.B > a > span:nth-child(1)',
                oldArea: 'body > div.S > div.B > a > span:nth-child(2)',
                oldBusinessCount: 'body > div.S > div.B > a > span:nth-child(3)',
                oldBusinessArea: 'body > div.S > div.B > a > span:nth-child(4)'
            },
            tableMap: {
                newTable: '#bgleft > table > tbody',
                oldTable: '#bgright > table > tbody'
            },
            tableHeader: tableHeader
        }
    } else {
        let newDataMapStart = 'body > div.con1.mg > div.con1r.f.xi16.cen > table > tbody > tr > td:nth-child(1)';
        let oldDataMapStart = 'body > div.con1.mg > div.con1r.f.xi16.cen > table > tbody > tr > td:nth-child(2)';
        return {
            dataMap: {
                newCount: newDataMapStart + '> span:nth-child(1)',
                newArea: newDataMapStart + '> span:nth-child(2)',
                newBusinessCount: newDataMapStart + '> span:nth-child(3)',
                newBusinessArea: newDataMapStart + '> span:nth-child(4)',
                oldCount: oldDataMapStart + '> span:nth-child(1)',
                oldArea: oldDataMapStart + '> span:nth-child(2)',
                oldBusinessCount: oldDataMapStart + '> span:nth-child(3)',
                oldBusinessArea: oldDataMapStart + '> span:nth-child(4)'
            },
            tableMap: {
                newTable: 'body > div.con2.mg > div.con2l.f > div.con2lx.mg2.xi12 > table > tbody',
                oldTable: 'body > div.con2.mg > div.con2l.r > div.con2lx.mg2.xi12 > table > tbody'
            },
            tableHeader: {
                '区县': 'name',
                '住宅套数': 'houseCount',
                '住宅面积（㎡）': 'houseArea',
                '总成交套数': 'allCount',
                '总成交面积（㎡）': 'allArea'
            }
        }
    }
}


function downloadHtml(date) {
    let orignalDataPath = path.join(ORIGNAL_DATA_PATH, date);
    return fsStat(orignalDataPath)
        .catch((error) => {
            return fsMkdir(orignalDataPath);
        })
        .then(() => {
            let filePath = path.join(orignalDataPath, date + '_' + Date.now() + '.html');
            let ws = fs.createWriteStream(filePath);
            return new Promise(function (resolve) {
                ws.on('close', function () {
                    resolve(filePath);
                })
                request({
                        uri: 'http://www.qdfd.com.cn/qdweb/realweb/indexnew.jsp'
                    })
                    .pipe(iconv.decodeStream('GBK'))
                    .pipe(iconv.encodeStream('utf8'))
                    .pipe(ws);
            })
        })
}

function explainHtml(filePath, date) {
    let file = fs.readFileSync(filePath, 'utf8');
    let data = {};
    file.replace('charset=GBK', 'charset=utf8');
    return new Promise(function (resolve, reject) {
        jsdom.env({
            html: file,
            src: [jquery],
            done: function (err, window) {
                let $ = window.$;
                let config = getConfig(date);
                let dataMap = config.dataMap;
                let tableMap = config.tableMap;
                data.date = date;
                for (let key in dataMap) {
                    if (!dataMap.hasOwnProperty(key)) {
                        continue;
                    }
                    data[key] = $(dataMap[key]).html();
                }
                data.newDetails = explainTable($, tableMap.newTable, config.tableHeader);
                data.oldDetails = explainTable($, tableMap.oldTable, config.tableHeader);
                return resolve(data);
            }
        })
    });
}

function explainTable($, table, tableHeader) {
    let details = [];
    let map = {};
    $(table)
        .children('tr')
        .each((index, ele) => {
            ele = $(ele);
            if (index == 0) {
                ele.children('td').each((index, ele) => {
                    map[index] = tableHeader[ele.innerHTML];
                })
            } else {
                let detail = {};
                details.push(detail);
                ele.children('td').each((index, ele) => {
                    detail[map[index]] = ele.innerHTML;
                });
            }
        })
    return details;
}

function writeFile(data, date) {
    let dataPath = path.join(DATA_PATH, date);
    return fsStat(dataPath)
        .catch((error) => {
            return fsMkdir(dataPath);
        })
        .then(() => {
            fs.writeFileSync(
                path.join(dataPath, `${date}_${Date.now()}.json`),
                JSON.stringify(data), {
                    encoding: 'utf8'
                }
            )
        })
}

function save(allData) {
    let saveDetails = [];
    let date = allData.date;
    let areaMap = [];
    let areaMapFind = [];
    areaMapFind.push({
        name: _.map(allData.newDetails, 'name'),
        type: 'new',
        level: 2
    });
    areaMapFind.push({
        name: _.map(allData.oldDetails, 'name'),
        type: 'old',
        level: 2
    });
    areaMapFind.push({
        name: '全市',
        type: 'new',
        level: 1
    });
    areaMapFind.push({
        name: '全市',
        type: 'old',
        level: 1
    });
    return Promise.each(areaMapFind, function (data) {
            return AreaMap.findOrCreate({
                    name: data.name,
                    level: data.level,
                    type: data.type
                })
                .then((data) => {
                    areaMap = areaMap.concat(data);
                })
        })
        .then(() => {
            for (let detail of allData.newDetails) {
                let area = _.find(areaMap, function (data) {
                    return data.type == 'new' && data.level == '2' &&
                        data.name == detail.name;
                });
                saveDetails.push({
                    date: date,
                    areaMapId: area.id,
                    houseCount: detail.houseCount,
                    houseArea: detail.houseArea,
                    allCount: detail.allCount,
                    allArea: detail.allArea
                })
            }
            for (let detail of allData.oldDetails) {
                let area = _.find(areaMap, function (data) {
                    return data.type == 'old' && data.level == '2' &&
                        data.name == detail.name;
                });
                saveDetails.push({
                    date: date,
                    areaMapId: area.id,
                    houseCount: detail.houseCount,
                    houseArea: detail.houseArea,
                    allCount: detail.allCount,
                    allArea: detail.allArea
                })
            }
            let newAllArea = _.find(areaMap, function (data) {
                return data.type == 'new' && data.level == 1;
            })
            saveDetails.push({
                date: date,
                areaMapId: newAllArea.id,
                houseCount: allData.newCount,
                houseArea: allData.newArea,
                allCount: parseInt(allData.newCount) + parseInt(allData.newBusinessCount),
                allArea: (parseFloat(allData.newArea) + parseFloat(allData.newBusinessArea)).toFixed(2)
            })
            let oldAllArea = _.find(areaMap, function (data) {
                return data.type == 'old' && data.level == 1;
            });
            saveDetails.push({
                date: date,
                areaMapId: oldAllArea.id,

                houseCount: allData.oldCount,
                houseArea: allData.oldArea,
                allCount: parseInt(allData.oldCount) + parseInt(allData.oldBusinessCount),
                allArea: (parseFloat(allData.oldArea) + parseFloat(allData.oldBusinessArea)).toFixed(2)
            });
            for (let detail of saveDetails) {
                detail.houseCount = detail.houseCount || 0;
                detail.houseArea = detail.houseArea || 0;
                detail.allCount = detail.allCount || 0;
                detail.allArea = detail.allArea || 0;
            }
            return DailyDataDb.bulkCreate(saveDetails);
        })
}

let date = moment().format('YYYY-MM-DD');

function downloadAndExplain(date) {
    downloadHtml(date)
        .then((data) => {
            // console.log(data);
            return explainHtml(data, date);
        })
        // explainHtml('/Users/mac/Documents/private/fang/orignalData/2017-04-06/2017-04-06.html', '2017-04-06')
        .then((data) => {
            return writeFile(data, date)
                .then(() => {
                    return save(data);
                })
        })
}

downloadAndExplain(date);

// let startDate = '2017-04-06';
// let endDate = '2017-05-03';
// let dateArr = [];
// while (startDate <= endDate) {
//     dateArr.push(startDate);
//     startDate = moment(startDate).add(1, 'd').format('YYYY-MM-DD');
// }
// Promise.each(dateArr, function (date) {
//     return fsReadDir(path.join(__dirname, './orignalData', date))
//         .then((data) => {
//             return explainHtml(path.join(__dirname, './orignalData', date, data[0]), date)
//                 .then((data) => {
//                     return writeFile(data, date)
//                         .then(() => {
//                             return save(data);
//                         })
//                 })
//         })
// })