module.exports = function (sequelize, DataTypes) {
    var AreaMap = sequelize.define('AreaMap', {
        //区域名称
        name: {
            type: DataTypes.TEXT
        },
        // 等级，1为全局 2为区域
        level: {
            type: DataTypes.INTEGER
        },
        //类型，new为新房，old为二手房
        type: {
            type: DataTypes.TEXT
        }
    }, {
        freezeTableName: true,
        tableName: 'areaMap'
    });
    return AreaMap;
};