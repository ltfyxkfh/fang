module.exports = function (sequelize, DataTypes) {
    var DailyData = sequelize.define('DailyData', {
        //日期
        date: {
            type: DataTypes.TEXT
        },
        // 区域ID
        areaMapId: {
            type: DataTypes.INTEGER
        },
        // 值
        houseCount: {
            type: DataTypes.INTEGER
        },
        houseArea: {
            type: DataTypes.DECIMAL
        },
        allCount: {
            type: DataTypes.INTEGER
        },
        allArea: {
            type: DataTypes.DECIMAL
        }
    }, {
        freezeTableName: true,
        tableName: 'dailyData'
    });
    return DailyData;
};